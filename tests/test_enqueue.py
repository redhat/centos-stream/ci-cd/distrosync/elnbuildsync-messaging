import pytest
from jsonschema.exceptions import ValidationError

from elnbuildsync_messages.enqueue import EnqueueV1


def test_enqueue_message():
    body = {
        "build_id": 1457909,
        "name": "http-parser",
        "tag_id": 6446,
        "instance": "primary",
        "tag": "f31-updates-pending",
        "user": "elnbuildsync",
        "version": "2.9.3",
        "owner": "sgallagh",
        "release": "1.fc31",
    }

    msg = EnqueueV1(body=body)
    msg.validate()
    assert msg.build_id == 1457909
    assert msg.name == "http-parser"
    assert msg.tag == "f31-updates-pending"
    assert msg.tag_id == 6446
    assert msg.instance == "primary"
    assert msg.user == "elnbuildsync"
    assert msg.owner == "sgallagh"
    assert msg.version == "2.9.3"
    assert msg.release == "1.fc31"

    assert msg.agent_name == "elnbuildsync"
    assert msg.summary == "http-parser-2.9.3-1.fc31 was enqueued for rebuilding by elnbuildsync"
    assert str(msg) == msg.summary


def test_enqueue_bad_message():
    # Missing 'build_id'
    body = {
        "name": "http-parser",
        "tag_id": 6446,
        "instance": "primary",
        "tag": "f31-updates-pending",
        "user": "bodhi",
        "version": "2.9.3",
        "owner": "sgallagh",
        "release": "1.fc31",
    }

    msg = EnqueueV1(body=body)
    pytest.raises(ValidationError, msg.validate)
