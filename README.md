# elnbuildsync-fedoramessaging messages

A schema package for [elnbuildsync-fedoramessaging](https://gitlab.com/redhat/centos-stream/ci-cd/distrosync/elnbuildsync-messaging).

See the [detailed documentation](https://fedora-messaging.readthedocs.io/en/latest/messages.html) on packaging your schemas.
